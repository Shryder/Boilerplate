var mongoose = require('mongoose')
  , Schema = mongoose.Schema
  , autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);

var userSchema = new Schema({
    id: Number,
    username: String,
    email: String,
    password: String,
    joinDate: Date,
    online: Boolean
}, { collection: 'users' });

userSchema.plugin(autoIncrement.plugin, {model: 'users', field: 'id', startAt: 1});

module.exports = mongoose.model('users', userSchema);